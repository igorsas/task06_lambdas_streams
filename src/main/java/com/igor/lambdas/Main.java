package com.igor.lambdas;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Threeable maxValue = (a, b, c) -> {
            if(a > b){
                if(a > c){
                    return a;
                }else{
                    return c;
                }
            }else{
                if(b > c) {
                    return b;
                }else {
                    return c;
                }
            }
        };

        Threeable average = (a, b, c) -> (a+b+c)/3;

        System.out.println("Input first number: ");
        int a = scanner.nextInt();
        System.out.println("Input second number: ");
        int b = scanner.nextInt();
        System.out.println("Input third number: ");
        int c = scanner.nextInt();

        System.out.println("Max value: " + maxValue.operation(a, b, c));
        System.out.println("Average number: " + average.operation(a, b, c));
    }
}
