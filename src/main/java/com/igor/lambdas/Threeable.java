package com.igor.lambdas;

@FunctionalInterface
public interface Threeable {
    public int operation(int a, int b, int c);
}
