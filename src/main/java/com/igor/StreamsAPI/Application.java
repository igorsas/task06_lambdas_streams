package com.igor.StreamsAPI;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(30);
        for (int i = 0; i < 30; i++) {
            list.add(i+2);
        }

        MethodAPI statistic = (l) -> {
            IntSummaryStatistics statistics = l.stream()
                    .mapToInt((x) -> x)
                    .summaryStatistics();
            System.out.println(statistics);

            int sum = l.stream()
                    .mapToInt((x) -> x)
                    .sum();
            System.out.println("Sum: " + sum);

            OptionalInt s = l.stream()
                    .mapToInt((x) -> x)
                    .reduce((a, b) -> a + b);

            System.out.println("Sum: " + s);
        };




        statistic.doSmth(list);
        MethodAPI print = new Application()::printCountOfValueBiggerThenAvarage;
        print.doSmth(list);

    }


    public void printCountOfValueBiggerThenAvarage(List<Integer> list){
        Integer average = (int)list.stream()
                .mapToInt(x -> x)
                .average().getAsDouble();

        long count = list.stream()
                .mapToInt(x -> x)
                .filter(a -> a > (int)average)
                .count();
        System.out.println("Count of value bigger then average: " + count);
    }
}
