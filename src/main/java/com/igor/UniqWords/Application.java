package com.igor.UniqWords;

import java.util.*;

public class Application {
    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private List<String> list;
    private Scanner scanner = new Scanner(System.in);

    public Application(){
        list = new ArrayList<>();

        menu = new LinkedHashMap<>();
        menu.put("1", "1 - add elements");
        menu.put("2", "2 - do list of uniq word");
        menu.put("3", "3 - sorted list with words");
        menu.put("4", "4 - print word count");
        menu.put("5", "5 - print chars count, except uppercase chars");
        menu.put("Q", "Q - quit");

        Methods methods = new Methods(list);

        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", methods::addElements);
        methodMenu.put("2", methods::uniq);
        methodMenu.put("3", methods::sort);
        methodMenu.put("4", methods::printWordCount);
        methodMenu.put("5", methods::printWordCountWithoutUpperCaseChars);
    }

    private void outputMenu(){
        System.out.println("\nMENU:");
        for (String s : menu.values()){
            System.out.println(s);
        }
    }

    private void printList(){
        System.out.println("Your list\n");
        for (String s: list){
            System.out.print(s + " ");
        }
        System.out.println();
    }

    public void showMenu(){
        String keyMenu, name;
        do {
            outputMenu();
            System.out.println("Choose point");
            keyMenu = scanner.nextLine();
            try {
                methodMenu.get(keyMenu).print();
            }catch (Exception ignored){}
        }while (!keyMenu.toUpperCase().equals("Q"));
        printList();
    }

    public static void main(String[] args) {
        Application application = new Application();
        application.showMenu();
    }
}
