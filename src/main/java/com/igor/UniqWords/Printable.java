package com.igor.UniqWords;

import java.io.IOException;

@FunctionalInterface
public interface Printable {
    void print() throws IOException;
}
