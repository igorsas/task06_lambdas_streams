package com.igor.UniqWords;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Methods {
    private List<String> list;
    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    public Methods(List<String> list){
        this.list = list;
    }

    public void addElements() throws IOException {
        System.out.println("Enter elements(empty line for quit)");
        while (true){
            String word = reader.readLine();
            if(word.isEmpty()){
                break;
            }
            list.add(word);
        }
    }

    public void sort(){
        List<String> s = list.stream().sorted(String::compareTo).collect(Collectors.toList());
        list.clear();
        list.addAll(s);
    }

    public void uniq(){
        List<String> s = list.stream().distinct().collect(Collectors.toList());
        list.clear();
        list.addAll(s);
    }

    public void printWordCount(){
        List<String> tmpList = new ArrayList<>(list.size());
        for(String s : list){
            tmpList.add(s + "-" + list.stream().filter(a -> a.equals(s)).count());
        }
        list.clear();
        list.addAll(tmpList);
        this.uniq();
    }

    public void printWordCountWithoutUpperCaseChars(){
        List<String> tmpList = new ArrayList<>();
        String str = "";
        for (String s : list){
            str += s;
        }
        IntStream chars = str.chars();
        for (int i = 0; i < str.length(); i++) {
            String finalStr = str;
            int finalI = i;
            if(Character.isUpperCase(finalStr.charAt(i)))
                continue;
            tmpList.add("" + str.charAt(i) + "-" + chars.filter(a -> finalStr.charAt(finalI) == a).count());
        }
        list.clear();
        list.addAll(tmpList);
        this.uniq();
    }
}
