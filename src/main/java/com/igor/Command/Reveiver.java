package com.igor.Command;

import java.text.SimpleDateFormat;
import java.util.*;

public class Reveiver {
    Map<String, String> menu;
    Map<String, Command> menuCommand;
    Scanner scanner = new Scanner(System.in);
    public Reveiver(){
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - help");
        menu.put("2", "2 - date now");
        menu.put("3", "3 - order pizza");
        menu.put("4", "4 - welcome");
        menu.put("Q", "Q - exit");
        menuCommand = new LinkedHashMap<>();
        menuCommand.put("1", (name) -> System.out.println("I'll help you. " + name + " don't give up."));
        menuCommand.put("2", new Command() {
            @Override
            public void execute(String name) {
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                System.out.println("Hello "+ name + ". Now is time: " + sdf.format(cal.getTime()));
            }
        });
        menuCommand.put("3", this::orderPizza);
        menuCommand.put("4", new WelcomeCommand());
    }

    public void orderPizza(String name){
        System.out.println("Pizza " + name + " sent. Please wait for your pizza");
    }

    private void outputMenu(){
        System.out.println("\nMENU:");
        for (String s : menu.values()){
            System.out.println(s);
        }

    }

    public void showMenu(){
        String keyMenu, name;
        do {
            outputMenu();
            System.out.println("Choose point");
            keyMenu = scanner.nextLine().toUpperCase();
            if(!keyMenu.equals("Q")){
                System.out.println(keyMenu.equals("3") ? "Write pizza name, please" : "Write your name, please");
            } else{
                break;
            }
            name = scanner.nextLine();
            try {
                menuCommand.get(keyMenu).execute(name);
            }catch (Exception ignored){}
        }while (!keyMenu.equals("Q"));
    }

    public static void main(String[] args) {
        Reveiver reveiver = new Reveiver();
        reveiver.showMenu();
    }
}
