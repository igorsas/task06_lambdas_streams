package com.igor.Command;

public class WelcomeCommand implements Command {
    @Override
    public void execute(String argument) {
        System.out.println("You're welcome, " + argument);
    }
}
