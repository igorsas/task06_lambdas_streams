package com.igor.Command;
@FunctionalInterface
public interface Command {
    public void execute(String argument);
}
